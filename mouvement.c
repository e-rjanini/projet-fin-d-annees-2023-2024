#include "joueur.h"
#include "mouvement.h"

/*Auteur : Matéo Guillou
Date : 28 mai 2024
Resume : fonction qui permet de faire avancer des pions sur le plateau
Entree : Tableau de Joueur, indice du joueur, indice du pion, valeur du dé
Sortie : un entier indiquant si le joueur a mangé le pion adverse et doit donc avancer de 20 cases*/
int Avancer(Joueur* J, int indiceJ, int indice_pion, int val_de) {
    int i = J[indiceJ].tabPosition[indice_pion];
    int indiceJ_ennemi;
    int aAvance = 0;
    if ((i > 0)&&(i < 69)) {
        if (J[indiceJ].tabPosition[indice_pion] + val_de > 68) {
            val_de = -1*(68-(J[indiceJ].tabPosition[indice_pion] + val_de));
            J[indiceJ].tabPosition[indice_pion] = val_de;
        }
        else {
            J[indiceJ].tabPosition[indice_pion] += val_de;
        }
        int peut_passer = 1;
        if (i + val_de < 69) {
            for (int k = i ; k < J[indiceJ].tabPosition[indice_pion] + 1; k++) {
                if ((peut_passer)&&(!PeutPasser(J, k, indiceJ))) {
                    J[indiceJ].tabPosition[indice_pion] = k-1;
                    peut_passer = 0;
                }
            }
        }
        else {
            for (int k = i ; k < 69 ; k++) {
                if ((peut_passer)&&(!PeutPasser(J, k, indiceJ))) {
                    J[indiceJ].tabPosition[indice_pion] = k-1;
                    peut_passer = 0;
                }
            }
            if (peut_passer) {
                for (int k = 1 ; k < J[indiceJ].tabPosition[indice_pion] + 1; k++) {
                    if ((peut_passer)&&(!PeutPasser(J, k, indiceJ))) {
                        J[indiceJ].tabPosition[indice_pion] = k-1;
                        if (J[indiceJ].tabPosition[indice_pion] == 0) {
                            J[indiceJ].tabPosition[indice_pion] = 68;
                        }
                        peut_passer = 0;
                    }
                }
            }
        }
    }
    indiceJ_ennemi = PionSurCase(J, indice_pion, indiceJ);
    if (indiceJ_ennemi != -1) {
        aAvance = Manger(J, indice_pion, indiceJ, indiceJ_ennemi);
    }
    return aAvance;
}

/*Auteur : Matéo Guillou
Date : 28 mai 2024
Resume : fonction qui permet de voir si un barrage se situe sur le chemin d'un pion qui avance
Entree : Tableau de Joueur, position du pion, indice du joueur
Sortie : 1 si le pion peut passer, 0 si il y a un barrage sur le chemin*/

int PeutPasser(Joueur* J, int position_pion, int indiceJ) {
    int peut_passer = 1;
    for (int i = 0 ; i < 4 ; i++) {
        int pions_mm_endroit = 0;
        for (int j = 0 ; j < 4 ; j++) {
            if ((J[i].tabPosition[j] == position_pion)&&(i != indiceJ)) {
                pions_mm_endroit++;
            }
            if (pions_mm_endroit > 1) {
                peut_passer = 0;
            }
        }
    }
    return peut_passer;
}



/*Auteur : Emil Richard, janini raphael*/
/*Date : 23 mai 2024 update- 16/6/2024*/
/*Resume : Fonction qui verfifie la couleur d'un pion et l'envoie dans la base (sauf si elle est pleine)*/
/*Entree : Le joueur est sont numero (0 ou 1 ou 2 ou 3)*/
/*Sortie : Aucune*/
void RetourBase(Joueur joueur, int indice_du_pion){
    int tabDeDebutDeBase[] = {101,109,105,97};
    int indice = tabDeDebutDeBase[joueur.couleurs], pionPose=0;
    while (!pionPose && !(nbPionsALaBase(joueur)==4)){
        if ((joueur.tabPosition[0] != indice)&& (joueur.tabPosition[1] != indice) && (joueur.tabPosition[2] != indice) && (joueur.tabPosition[3] != indice)){
            joueur.tabPosition[indice_du_pion] = indice;
            pionPose = 1;
        }
        indice++;
    }
}


/*Auteur : Matéo Guillou
Date : 29 mai 2024
Resume : fonction qui permet de poser un pion à l'arrivée dans une case qui n'est pas occuppée
Entree : Tableau de Joueur, indice du joueur, indice du pion, indice première valeur arrivée, indice final arrivée +1
Sortie : aucune*/

void Boucle(Joueur* J, int indiceJ, int indice_pion, int i1, int i2) {
    int est_pose = 1;
    for (int j = i1 ; j < i2 ; j++) {
        if (est_pose == 0) {
            est_pose = 1;
        }
        for (int k = 0 ; k < 4 ; k++) {
            if (J[indiceJ].tabPosition[k] == j) {
                est_pose = 0;
            }
        }
        if(est_pose == 1) {
            J[indiceJ].tabPosition[indice_pion] = j;
            est_pose = 2;
        }
    }
}

/*Auteur : Matéo Guillou
Date : 29 mai 2024
Resume : fonction qui permet de mettre un pion à l'arrivée
Entree : Tableau de Joueur, indice du joueur, indice du pion
Sortie : aucune*/

void PrendPionMetALaFin(Joueur* J, int indiceJ, int indice_pion) {
    int i = 0;
    switch(J[indiceJ].couleurs) {
        case ROUGE : i = 1;break;
        case BLEU : i = 2;break;
        case VERT : i = 3;break;
        case JAUNE : i = 4;break;
        default : printf("Erreur de segmentation");
    }
    if (i == 1) {
        Boucle(J, indiceJ, indice_pion, 113, 117);
    }
    else if (i == 2) {
        Boucle(J, indiceJ, indice_pion, 117, 121);
    }
    else if (i == 3) {
        Boucle(J, indiceJ, indice_pion, 121, 125);
    }
    else if (i == 4) {
        Boucle(J, indiceJ, indice_pion, 125, 129);
    }
}


/*Auteur : Matéo Guillou, raphael janini
Date : 29 mai 2024 -update 16/6/2024
Resume : fonction qui permet de faire avancer des pions sur les échelles (Partie finale du circuit)
Entree : Tableau de Joueur, indice du joueur, indice du pion, valeur du dé
Sortie : aucune*/
void AvancerEchelle(Joueur* J, int indiceJ, int indice_pion, int val_de) {
    int posInit = J[indiceJ].tabPosition[indice_pion];
    int tab2ndMarcheEchelle[] = {76,90,83,69};
    if (posInit<69){
        J[indiceJ].tabPosition[indice_pion] = tab2ndMarcheEchelle[indiceJ];
        posInit = J[indiceJ].tabPosition[indice_pion];
        val_de -= 1;
    }
    if (posInit+val_de == tab2ndMarcheEchelle[indiceJ]+7) {
        PrendPionMetALaFin(J, indiceJ, indice_pion);
    }else {
        if (posInit+val_de < tab2ndMarcheEchelle[indiceJ]+7){
            J[indiceJ].tabPosition[indice_pion] += val_de;
        }
    }
}

/*Auteur : Emil Richard, Matéo Guillou*/
/*Date : 28 mai 2024*/
/*Resume : Fonction qui renvoi un pion mangé a sa base*/
/*Entree : Les joueurs, l'indice du pion qui vien de jouer est l'indice de ce joueur, pour finir l'indice du joueur du pion que l'on veut manger*/
/*Sortie : Un entier indiquant si le joueur veut manger le pion adverse ou non */
int Manger(Joueur* joueur, int indice_pion_alliee, int indice_joueur_alliee, int indice_joueur_enemie){
    int VeutManger;
    if (joueur[indice_joueur_alliee].estOrdi[0] == 0) {
        do {
            printf("Voulez vous manger le pion adverse ? (Oui : 1, Non : 0)\n");
            scanf("%d", &VeutManger);
        } while ((VeutManger != 1)&&(VeutManger != 0));
    }
    else {
        VeutManger = 1;
    }
    int posInvincible[] = {34,39,29,46,22,51,17,56,12,63,5,68};
    int invincible = 0;
    if (VeutManger) {
        for (int i = 0; i < 12; i++){
            if (joueur[indice_joueur_alliee].tabPosition[indice_pion_alliee] == posInvincible[i]){
                invincible = 1;
                printf("Vous ne pouvez pas manger ce pion, il est sur une case d'invincibilité !\n");
                VeutManger = 0;
            }
        }
        for (int j = 0; j < 4; j++){
            if ((joueur[indice_joueur_alliee].tabPosition[indice_pion_alliee] == joueur[indice_joueur_enemie].tabPosition[j]) && !invincible){
                RetourBase(joueur[indice_joueur_enemie], j);
            }
        }
    }
    return VeutManger;
}

/*Auteur : Raphael janini*/
/*Date : 17/06/2024*/
/*Resume : Fonction qui renvoi un pion mangé a sa base sans prendre en compt les case Invincible*/
/*Entree : Les joueurs,la position du pion ou des pions que l'on veut manger,l'indice du joueur du pion que l'on veut manger*/
/*Sortie : Un entier indiquant si le joueur a manger le pion adverse ou non */
int MangerInvincible(Joueur* tabJoueur,int indiceJ,int pos){
    int AMange = 0;
    for (int i = 0; i < 4; i++){
        if ( i != indiceJ ){
            for (int j = 0; j < 4; j++){
                if ( pos == tabJoueur[i].tabPosition[j] ) {
                    RetourBase(tabJoueur[i], j);
                    AMange = 1;
                }
            }
        }
    }
    return AMange;
}

/* Auteur : Janini raphael */
/* Date :  14/06/24 */
/* Résumé :  fonction qui teste si il y a un pion sur la case où vient d'arriver un pion si oui renvoie la personne a qui appartient le pion*/
/*Entree :  le tableau de joueur et l'indice correspondant au pion*/
/*Sortie : un entier correspondant à l'indice d'un joueur */
int PionSurCase(Joueur* tabJoueur, int indice_pion, int indice_joueur){
    int res = -1;
    for (int i=0;i<4;i++){
        if (i != indice_joueur){
            for (int j=0;j<4;j++){
                if (tabJoueur[i].tabPosition[j] == tabJoueur[indice_joueur].tabPosition[indice_pion]){
                    res = i;
                }
            }
        }
    }
    return res;
}