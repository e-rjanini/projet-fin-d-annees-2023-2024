#include "joueur.h"
#include "fonction.h"
#include "affichage.h"
#include "mouvement.h"
#include "affichage.h"
#define MAX 100

/*Auteur : Emil Richard, Matéo Guillou, raphael janini*/
/*Date : 22 mai 2024 -update 13/06/2024*/
/*Resume : fonction qui simule un de a six faces*/
/*Entree : aucune*/
/*Sortie : un nombre aleatoire entre 1 et 6 inclus*/
int de(){
    int resultat;
    for(int i = 0; i < 16; i++){
        system("clear");
        resultat = rand() % 6+1;
        printDe(resultat);
        system("sleep 0.1");
        if (i > 11){
            system("sleep 0.2");
        }if(i > 13){
            system("sleep 0.4");
        }
    }
    return resultat;
}

void emptyBuffer() {
  char c;
  while (((c = getchar()) != '\n') && (c != EOF));
}

/* Auteur : Thomas caudron */
/* Date :  22/05/24 */
/* Résumé :  alloue un joueur */
/* Entrée(s) :  Joueur */
/* Sortie(s) :  Joueur */
Joueur allouerJoueur ()
{   
    Joueur joueur;
    joueur.age = malloc(sizeof(int));
    joueur.estOrdi = malloc(sizeof(int));
    joueur.tabPosition=malloc(4*sizeof(int));
    joueur.nom=malloc(sizeof(char)*MAX);
    joueur.prenom=malloc(sizeof(char)*MAX);
    return joueur;
}

/* Auteur : Thomas caudron */
/* Date :  28/05/24 */
/* Résumé :  initialise un joueur */
/* Entrée(s) : un joueur et son numéro*/
/* Sortie(s) :  */


void initialiserJoueur (Joueur joueur, int nb)  
{
    system("clear");
    printf("------------------------------\n- INITIALISATION DU JOUEUR %d -\n------------------------------\n\n", nb);
    joueur.estOrdi[0] = 0;
    printf("Veuillez entrer votre Nom:   \n");
    scanf("%s",joueur.nom);
    emptyBuffer();
    printf("Veuillez entrer votre Prénom:   \n");
    scanf("%s",joueur.prenom);
    emptyBuffer();
    printf("Veuillez entrer votre Age:   \n");
    scanf("%d",&joueur.age[0]);
    emptyBuffer();
}

/*Auteur : Matéo Guillou
Date : 14 juin 2024
Resume : fonction qui permet d'initialiser un bot
Entree : Joueur, numéro de l'ordi
Sortie : rien*/

void InitialiserOrdi (Joueur joueur, int nb) {
    joueur.estOrdi[0] = 1;
    sprintf(joueur.nom, "%d",nb);
    sprintf(joueur.prenom, "%s", "Ordi");
    joueur.age[0] = rand() %100 + 1;
}

/* Auteur : Thomas caudron */
/* Date :  28et29/05/24 */
/* Résumé :  trie l'age d'un tableau de joueur et initialise les positions*/
/* Entrée(s) :  tableau de Joueur */
/* Sortie(s) :  */
void InitialiseCouleur (Joueur* joueur1)
{
    int i, j;
    Joueur temp;
    temp = allouerJoueur();
    for (i = 0; i < 3; i++) 
    {
        for (j = 0; j < 3 - i; j++) 
        {
            if (joueur1[j].age[0] > joueur1[j + 1].age[0]) 
            {
                temp = joueur1[j];
                joueur1[j] = joueur1[j + 1];
                joueur1[j + 1] = temp;
            }
        }
    }
    joueur1[0].couleurs=BLEU;
    joueur1[1].couleurs=JAUNE;
    joueur1[2].couleurs=VERT;
    joueur1[3].couleurs=ROUGE;

    joueur1[0].tabPosition[0]=101; //bleu 
    joueur1[0].tabPosition[1]=102;
    joueur1[0].tabPosition[2]=103;
    joueur1[0].tabPosition[3]=104;

    joueur1[1].tabPosition[0]=109; //jaune
    joueur1[1].tabPosition[1]=110;
    joueur1[1].tabPosition[2]=111;
    joueur1[1].tabPosition[3]=112;

    joueur1[2].tabPosition[0]=105; //vert
    joueur1[2].tabPosition[1]=106;
    joueur1[2].tabPosition[2]=107;
    joueur1[2].tabPosition[3]=108;

    joueur1[3].tabPosition[0]=97; //rouge
    joueur1[3].tabPosition[1]=98;
    joueur1[3].tabPosition[2]=99;
    joueur1[3].tabPosition[3]=100;
}

/* Auteur : Janini raphael */
/* Date :  13/06/24 */
/* Résumé :  teste si une person a tous ses pions dans l'arrivée*/
/* Entrée(s) :  tableau de Joueur */
/* Sortie(s) :  indice du gagnant si trouvé sinon renvoie 4*/
int conditionFinDeJeux(Joueur* tabJoueur){
    int res = 4;
    int nbPiondansbase;
    int tabDeDebutDeArrivee[] = {117,125,121,113};
    for (int i=0;i<4;i++){
        nbPiondansbase = 0;
        for (int j=0;j<4;j++){
            for (int k=0;k<4;k++){
                if (tabJoueur[i].tabPosition[j] == (tabDeDebutDeArrivee[i] + k)){
                    nbPiondansbase++;
                }
            }
        }
        if (nbPiondansbase == 4){
            res = i;
        }
    }
    return res;
}

/*Auteur : Matéo Guillou, raphael janini
Date : 13 juin 2024 
Resume : fonction qui permet de vérifier si tous les pions d'un joueur sont dans la base
Entree : Joueur
Sortie : 1 si tous les pions du joueur sont à la base, 0 sinon*/
int nbPionsALaBase(Joueur J) {
    int res = 0;
    int tabDeDebutDeBase[] = {101,109,105,97};
    int indiceJouer = J.couleurs;
    int indiceDebutDebase = tabDeDebutDeBase[indiceJouer];
    for (int i=0;i<4;i++){
        for (int j=0;j<4;j++){
            if (J.tabPosition[i] == indiceDebutDebase + j){
                res++;
            }
        }
    }
    return res;
}

/*Auteur : Matéo Guillou
Date : 13 juin 2024
Resume : fonction qui permet de choisir de monter sur l'échelle ou de faire un nouveau tour
Entree : Tableau de Joueur, entier (indice de joueur), entier (position du pion dans le tableau), entier (valeur du dé), entier (1 si la partie est rapide 0 sinon)
Sortie : Renvoie 1 si le joueur a décidé de continuer son chemin et de manger quelqu'un, 0 sinon*/
int ChoixEchelle(Joueur* J, int i, int PosChoisie, int ResDe, int estRapide) {
    int rep;
    int AMange = 0;
    do {
        printf("Voulez vous aller sur l'échelle (1) ou continuer (2)\n");
        if (J[i].estOrdi[0]) {
            rep = 1;
        } else {
        scanf("%d", &rep);
        }
        if ((rep != 1)&&(rep!=2)) {
            printf("erreur de saisie;");
        }
    } while ((rep != 1)&&(rep != 2));
    if (rep == 1) {
        AvancerEchelle(J, i, PosChoisie, ResDe);
    } else {
        AMange = Avancer(J, i, PosChoisie, ResDe);
    }
    return AMange;
}


/*Auteur : Guillou Matéo
Date : 17 juin 2024
Resume : Vérifie si le joueur a un barrage en place sur le plateau 
Entree : un joueur
Sortie : renvoie 1 si un barrage est en place, 0 sinon*/
int EstBarrage(Joueur joueur) {
    int compteur = 0;
    int res = 0;
    int tabval[] = {joueur.tabPosition[0], joueur.tabPosition[1], joueur.tabPosition[2], joueur.tabPosition[3]};
    for (int i = 0 ; i < 4 ; i++) {
        compteur = 0;
        for (int j = 0 ; j < 4 ; j++) {
            if (joueur.tabPosition[i] == tabval[j]) {
                compteur++;
            }
        }
        if (compteur > 1) {
            res = 1;
        }
    }
    return res;
}

/*Auteur : raphael janini
Date : 16 juin 2024
Resume : créé un tableau de tous les pion qui peuvent avancer selon les règles 
Entree : Tableau de Joueur,l'indice du joueur et  le resultas du dé
Sortie : un tableau dont la première valeure est sa taille puis les valeures suivantes sont les pions que l'on peut avancer de (ResDe) cases*/
int* creesTabPionPeutJouer(Joueur* J, int indiceJ, int ResDe){
    int* tabPionPeuJouer;
    int tab2ndMarcheEchelle[] = {76,90,83,69};
    tabPionPeuJouer = malloc(1*sizeof(int));
    int pionValable;
    int size=1;
    if ( ResDe==6 && EstBarrage(J[indiceJ]) ){
        int posBarrage;
        for (int j=0;j<4;j++){
            pionValable = 0;
            posBarrage = J[indiceJ].tabPosition[j];
            for (int k=0;k<4;k++){
                if ((k != j) && (posBarrage == J[indiceJ].tabPosition[k])){
                    pionValable = 1;
                }
            }
            if (pionValable){
                tabPionPeuJouer = realloc(tabPionPeuJouer, size*sizeof(int));
                tabPionPeuJouer[size] = j;
                size++;
            }
        }
    }else{
        for (int i=0;i<4;i++){
            pionValable = 0;
            if ((J[indiceJ].tabPosition[i] < 113 ) && ( J[indiceJ].tabPosition[i] > 96 ) && (ResDe==6)) {
                pionValable = 1;
            }else if ( ( J[indiceJ].tabPosition[i] < 69 )   &&   ( J[indiceJ].tabPosition[i] > 0 ) ){
                pionValable = 1;
            }else if (( J[indiceJ].tabPosition[i] < 97 )   &&   ( J[indiceJ].tabPosition[i] > 68 )) {
                if ( J[indiceJ].tabPosition[i]+ResDe <= tab2ndMarcheEchelle[indiceJ]+7){
                    pionValable = 1;
                }
            }
            if (pionValable){
                tabPionPeuJouer = realloc(tabPionPeuJouer, size*sizeof(int));
                tabPionPeuJouer[size] = i;
                size++;
            }
        }
    }
    tabPionPeuJouer[0] = size;
    return tabPionPeuJouer;
}

/*Auteur : Matéo Guillou ,raphael janini
Date : 16 juin 2024
Resume : fonction qui demande a l'utilisateur quel pion avancer. 
Entree : Tableau de Joueur, l'indice du joueur,le resultas du de et la vitess choisi par le joueur
Sortie : le choix du pion à avancer*/
int ChoisiPion (Joueur* J, int indiceJ, int ResDe, int estRapide) {
    int pionChoisie = -1;
    int* tabPionPeuJouer;
    tabPionPeuJouer = creesTabPionPeutJouer(J,indiceJ,ResDe);
    if (tabPionPeuJouer[0] != 1) {
        if (J[indiceJ].estOrdi[0]){
            pionChoisie = tabPionPeuJouer[(rand()%(tabPionPeuJouer[0]-1))+1];
        }
        else{
            int bonChoix=1;
            printf("Au tour de %s %s\n\n", J[indiceJ].prenom, J[indiceJ].nom);
            do {
                printf("Choisissez le pion que vous voulez jouer (écrire son numéro)\n");
                for (int i=1;i<tabPionPeuJouer[0];i++){
                    printf("Pion %d : %d\n",tabPionPeuJouer[i], J[indiceJ].tabPosition[tabPionPeuJouer[i]]);
                }
                scanf("%d", &pionChoisie);
                emptyBuffer();
                for (int i=1;i<tabPionPeuJouer[0];i++){
                    if (pionChoisie == tabPionPeuJouer[i]){
                        bonChoix = 0;
                    }
                }
            } while(bonChoix);
        }
    }else {
        printf( "%s %s n'a aucun pion jouable on saute sont tour \n\n", J[indiceJ].prenom, J[indiceJ].nom);
    }
    return pionChoisie;
}

/*Auteur : Matéo Guillou
Date : 13 juin 2024
Resume : fonction qui permet de faire tourner le jeu
Entree : Tableau de Joueur
Sortie : entier (indice du joueur qui a gagné)*/
int TourDeJeux(Joueur* J, int estRapide) {
    int i = 0;
    int ResDe;
    int PosChoisie;
    int AJoue;
    int AMange;
    int Nb6Cons = 0;
    int est36;
    int estBarrage;
    while (conditionFinDeJeux(J) == 4) {
        estBarrage = 0;
        est36 = 0;
        AJoue = 1;
        if (EstBarrage(J[i])) {
            estBarrage = 1;
        }
        system("clear");
        if (estRapide) {
            ResDe = rand() % 6 + 1;
        }
        else {
            ResDe = de();
        }
        if (ResDe == 6) {
            Nb6Cons++;
        } else {
            Nb6Cons = 0;
        }
        if (Nb6Cons == 3) {
            est36 = 1;
        }
        printParcheesi(J);
        printf("\n %s %s Vous avez fait : %d \n\n",J[i].prenom, J[i].nom, ResDe);
        if ((!(nbPionsALaBase(J[i]) == 4)) || ((nbPionsALaBase(J[i]) == 4) && (ResDe == 6))) {
            AMange = 0;
            PosChoisie = ChoisiPion(J, i, ResDe, estRapide);
            if ((!estRapide) && J[i].estOrdi[0]) {system("sleep 2");}
            if (PosChoisie != -1){
                if ((J[i].tabPosition[PosChoisie] < 97) && (J[i].tabPosition[PosChoisie] > 68)) {
                    AvancerEchelle(J, i, PosChoisie, ResDe);
                }
                else if ((J[i].tabPosition[PosChoisie] < 69)&&(J[i].tabPosition[PosChoisie] > 0)) {
                    int tabDebutEchelle[] = {17,68,51,34};
                    if ( J[i].tabPosition[PosChoisie] == tabDebutEchelle[i]) {
                        AMange = ChoixEchelle(J, i, PosChoisie, ResDe, estRapide);
                    }else {
                        AMange = Avancer(J, i, PosChoisie, ResDe);
                    }
                }
                else if ((J[i].tabPosition[PosChoisie] < 113)&&(J[i].tabPosition[PosChoisie] > 96)) {
                    int tabDeDebutDeParcour[] = {22,5,56,39};
                    J[i].tabPosition[PosChoisie] = tabDeDebutDeParcour[i];
                    AMange = MangerInvincible(J, i, tabDeDebutDeParcour[i]);
                }
                if (AMange) {
                    system("clear");
                    printParcheesi(J);
                    printf("Vous allez avancer de 20 cases avec le pion de votre choix ! \n");
                    if (!estRapide) {
                        system("sleep 2");
                    }
                    PosChoisie = ChoisiPion(J, i, 20, estRapide);
                    Avancer(J, i, PosChoisie, 20);
                }
            }
        } else if (!est36){
            AJoue = 0;
            printf("Vous ne pouvez pas jouer, passez votre tour\n");
            if (!estRapide) {
                system("sleep 0.3");
            }
        }
        if (AJoue) {
            printParcheesi(J);
            printf("\n %s %s Vous avez fait : %d\n\n",J[i].prenom, J[i].nom, ResDe);
            if (!estRapide) {
                system("sleep 3");
            }
            else {
                system("sleep 0.1");
            }
        }
        if (ResDe == 6 && EstBarrage(J[i]) && estBarrage) {
            printf("Vous avez fait 6 et deviez casser votre barrage, votre pion retourne à la base\n");
            RetourBase(J[i], PosChoisie);
        }
        if (est36) {
            RetourBase(J[i], PosChoisie);
        }
        if ((ResDe != 6)||est36||estBarrage) {
            Nb6Cons = 0;
            if (i == 3) {
                i = 0;
            } else {
                i++;
            }
        }

    }
    return (conditionFinDeJeux(J));
}