#ifndef __joueur_H_
#define __joueur_H_

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

/* Auteur : raphael */
/* Date : 22 may 2024 8:32*/
/* Résumé :  enumeration des couleurs possible*/
enum COULEURS { BLEU , JAUNE , VERT , ROUGE};

/* Auteur : raphael */
/* Date : 22 may 2024  8:31*/
/* Résumé : structure du joueur avec tableau des positions de chaque pion 
    - TabPosition : 97-100 ; 101-104 ; 105-108 ; 109-112 pion est dans la base
                    1-68 pion est dans le tableau de jeu
                    69-75 pion est sur les cases d'échelle Rouges
                    76-82 pion est sur les cases d'échelle Bleus
                    83-89 pion est sur les cases d'échelle Vertes
                    90-96 pion est sur les cases d'échelle Jaunes
*/
struct Joueur {
    int* tabPosition;
    char* nom;
    char* prenom;
    int* age;
    enum COULEURS couleurs;
    int* estOrdi;
};
typedef struct Joueur Joueur;

#endif
