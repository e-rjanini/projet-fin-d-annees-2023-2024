#include "fonction.h"
#include "affichage.h"
#include "joueur.h"
#include "mouvement.h"
#include <stdlib.h>
#include <stdio.h>

int main() {
    srand(time(NULL));
    Joueur* tabJoueur;// = Test();
    tabJoueur = malloc(4*sizeof(Joueur));
    int NbJoueurs = 5;
    do {
        system("clear");
        printf("Combien de Joueurs dans la partie ?\n");
        scanf("%d", &NbJoueurs);
        if ((NbJoueurs > 4)||(NbJoueurs < 0)) {
            printf("Erreur de saisie, le nombre de joueurs doit être compris entre 0 et 4\n");
        }
    } while ((NbJoueurs > 4)||(NbJoueurs < 0));
    for (int i = 0;i<NbJoueurs;i++){
        tabJoueur[i] = allouerJoueur();
        initialiserJoueur(tabJoueur[i], i+1);
    }
    int c = 0;
    for (int j = NbJoueurs ; j < 4 ; j++) {
        c++;
        tabJoueur[j] = allouerJoueur();
        InitialiserOrdi(tabJoueur[j], c);
    }
    int estRapide;
    system("clear");
    printf("\n----------\n- RESUME -\n----------\n\n");
    printf("Nom : %s Prenom : %s estOrdi : %d\n", tabJoueur[0].nom, tabJoueur[0].prenom, tabJoueur[0].estOrdi[0]);
    printf("Nom : %s Prenom : %s estOrdi : %d\n", tabJoueur[1].nom, tabJoueur[1].prenom, tabJoueur[1].estOrdi[0]);
    printf("Nom : %s Prenom : %s estOrdi : %d\n", tabJoueur[2].nom, tabJoueur[2].prenom, tabJoueur[2].estOrdi[0]);
    printf("Nom : %s Prenom : %s estOrdi : %d\n\n", tabJoueur[3].nom, tabJoueur[3].prenom, tabJoueur[3].estOrdi[0]);
    do {
        printf("Voulez vous un mode ultra rapide ? Y(1)/N(0)\n");
        scanf("%d", &estRapide);
    } while ((estRapide != 1) && (estRapide != 0));
    InitialiseCouleur(tabJoueur);
    printf("joueur1[0].tabPosition[0] : %d\n", tabJoueur[0].tabPosition[0]);
    TourDeJeux(tabJoueur, estRapide);
    
    printf("bravo %s %s tu a gagne\n",tabJoueur[conditionFinDeJeux(tabJoueur)].prenom,tabJoueur[conditionFinDeJeux(tabJoueur)].nom);
    return 0;
}