#ifndef __mouvement_H_
#define __mouvement_H_
#include "joueur.h"
#include "fonction.h"
#include "affichage.h"

int PeutPasser(Joueur* J, int position_pion, int indiceJ);

int Avancer(Joueur* J, int indiceJ, int indice_pion, int val_de);

void RetourBase(Joueur Joueur, int indice_du_pion);

void Boucle(Joueur* J, int indiceJ, int indice_pion, int i1, int i2);

void PrendPionMetALaFin(Joueur* J, int indiceJ, int indice_pion);

int Manger(Joueur* joueur, int indice_pion_alliee, int indice_joueur_alliee, int indice_joueur_enemie);

int MangerInvincible(Joueur* tabJoueur,int indiceJ,int pos);

void AvancerEchelle(Joueur* J, int indiceJ, int indice_pion, int val_de);

int PionSurCase(Joueur* tabJoueur, int indice_pion, int indice_joueur);
#endif