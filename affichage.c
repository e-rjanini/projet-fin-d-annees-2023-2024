#include "joueur.h"
#include "affichage.h"


// Ordre : ROUGE BLEU VERT JAUNE 

//ECHELLES :    69-75 ; 76-82 ; 83-89 ; 90-96
//BASES :       97-100 ; 101-104 ; 105-108 ; 109-112
//ARRIVEES :    113-116 ; 117-120 ; 121-124 ; 125-128

/*Auteur : Matéo Guillou
Date : 22-23 mai 2024
Resume : fonction qui affiche le plateau de jeu à partir des positions des pions
Entree : Tableau de Joueur
Sortie : aucune*/
void printParcheesi(Joueur* J) {
    system("clear");
    int nt = 129;
    int* T;
    T = malloc(nt*sizeof(int));
    for (int i = 0 ; i < nt ; i++) {
        T[i] = i;
    }
    for (int i = 0 ; i < 4 ; i++) {
        for (int j = 0 ; j < 4 ; j++) {
            switch(J[i].couleurs) {
                case ROUGE : T[J[i].tabPosition[j]] = -1; break;
                case BLEU : T[J[i].tabPosition[j]] = -2; break;
                case VERT : T[J[i].tabPosition[j]] = -3; break;
                case JAUNE : T[J[i].tabPosition[j]] = -4; break;
                default : printf("Erreur : Pas de couleur\n");
            }
        }
    }

    char** C;
    C = malloc(nt*sizeof(char*));
    for (int i = 0 ; i < nt ; i++) {
        C[i] = malloc(50*sizeof(char));
        char c[100];
        if (T[i] == i) {
            if((i < 10) && (i > 0)) {
                sprintf(c, "0%d", i);
            }
            else if ((i < 69) && (i > 0)) {
                sprintf(c, "%d", i);
            }
            else if (((i > 68)&&(i < 76))||((i > 96)&& (i<101))||((i < 117)&&(i > 112))) {
                sprintf(c, "🟥");
            }
            else if (((i > 75)&&(i < 83))||((i > 100)&&(i < 105))||((i > 116)&&(i < 121))) {
                sprintf(c, "🟦");
            }
            else if (((i > 82)&&(i < 90))||((i > 104)&&(i < 109))||((i > 120)&&(i<125))) {
                sprintf(c, "🟩");
            }
            else if (((i > 89)&&(i < 97))||((i > 108)&&(i < 113))||((i>124)&&(i<129))) {
                sprintf(c, "🟨");
            }
        }
        else {
            switch(T[i]) {
                case -1 : sprintf(c, "🔴"); break;
                case -2 : sprintf(c, "🔵"); break;
                case -3 : sprintf(c, "🟢"); break;
                case -4 : sprintf(c, "🟡"); break;
                default : printf("Erreur de segmentation\n");
            }
        }
        strcpy(C[i], c);
    }
    printf("🟥🟥🟥🟥🟥           ⬜ %s ⬜ ⬛ %s ⬛ ⬜ %s ⬜           🟦🟦🟦🟦🟦\n", C[35], C[34], C[33]);
    printf("🟥%s🟥%s🟥           ⬜ %s ⬜ 🟥 %s 🟥 ⬜ %s ⬜           🟦%s🟦%s🟦\n", C[97], C[98], C[36], C[69], C[32], C[101], C[102]);
    printf("🟥🟥🟥🟥🟥           ⬜ %s ⬜ 🟥 %s 🟥 ⬜ %s ⬜           🟦🟦🟦🟦🟦\n", C[37], C[70], C[31]);
    printf("🟥%s🟥%s🟥           ⬜ %s ⬜ 🟥 %s 🟥 ⬜ %s ⬜           🟦%s🟦%s🟦\n", C[99], C[100], C[38], C[71], C[30], C[103], C[104]);
    printf("🟥🟥🟥🟥🟥           ⬛ %s ⬛ 🟥 %s 🟥 ⬛ %s ⬛           🟦🟦🟦🟦🟦\n", C[39], C[72], C[29]);
    printf("                     ⬜ %s ⬜ 🟥 %s 🟥 ⬜ %s ⬜              \n",C[40], C[73], C[28]);
    printf("                     ⬜ %s ⬜ 🟥 %s 🟥 ⬜ %s ⬜              \n",C[41], C[74], C[27]);
    printf("⬜⬜⬜⬜⬜⬜⬛⬜⬜⬜ ⬜ %s ⬜ 🟥 %s 🟥 ⬜ %s ⬜ ⬜⬜⬜⬛⬜⬜⬜⬜⬜⬜\n", C[42], C[75], C[26]);
    printf("%s %s %s %s %s %s %s %s 🟩🟥🟥🟥🟥🟥🟥🟥🟥🟦 %s %s %s %s %s %s %s %s\n", C[50], C[49], C[48], C[47], C[46], C[45], C[44], C[43], C[25], C[24], C[23], C[22], C[21], C[20], C[19], C[18]);
    printf("⬜⬜⬜⬜⬜⬜⬛⬜⬜⬜⬜⬜🟩🟩🟥🟥🟥🟥🟥🟥🟦🟦⬜⬜⬜⬜⬜⬛⬜⬜⬜⬜⬜⬜\n");
    printf("⬛🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩%s%s%s%s%s%s🟦🟦🟦🟦🟦🟦🟦🟦🟦🟦🟦🟦🟦⬛\n", C[121], C[113], C[114], C[115], C[116], C[117]);
    printf("%s %s %s %s %s %s %s %s 🟩🟩%s%s🟨🟥%s%s🟦🟦 %s %s %s %s %s %s %s %s\n", C[51], C[83], C[84], C[85], C[86], C[87], C[88], C[89], C[122], C[123], C[118], C[119], C[82], C[81], C[80], C[79], C[78], C[77], C[76], C[17]);
    printf("⬛🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩%s%s%s%s%s%s🟦🟦🟦🟦🟦🟦🟦🟦🟦🟦🟦🟦🟦⬛\n", C[124], C[125], C[126], C[127], C[128], C[120]);
    printf("⬜⬜⬜⬜⬜⬜⬛⬜⬜⬜⬜⬜🟩🟩🟨🟨🟨🟨🟨🟨🟦🟦⬜⬜⬜⬜⬜⬛⬜⬜⬜⬜⬜⬜\n");
    printf("%s %s %s %s %s %s %s %s 🟩🟨🟨🟨🟨🟨🟨🟨🟨🟦 %s %s %s %s %s %s %s %s\n", C[52], C[53], C[54], C[55], C[56], C[57], C[58], C[59], C[9], C[10], C[11], C[12], C[13], C[14], C[15], C[16]);
    printf("⬜⬜⬜⬜⬜⬜⬛⬜⬜⬜ ⬜ %s ⬜ 🟨 %s 🟨 ⬜ %s ⬜ ⬜⬜⬜⬛⬜⬜⬜⬜⬜⬜\n", C[60], C[96], C[8]);
    printf("                     ⬜ %s ⬜ 🟨 %s 🟨 ⬜ %s ⬜              \n",C[61], C[95], C[7]);
    printf("                     ⬜ %s ⬜ 🟨 %s 🟨 ⬜ %s ⬜              \n",C[62], C[94], C[6]);
    printf("🟩🟩🟩🟩🟩           ⬛ %s ⬛ 🟨 %s 🟨 ⬛ %s ⬛           🟨🟨🟨🟨🟨\n", C[63], C[93], C[5]);
    printf("🟩%s🟩%s🟩           ⬜ %s ⬜ 🟨 %s 🟨 ⬜ %s ⬜           🟨%s🟨%s🟨\n", C[105], C[106], C[64], C[92], C[4], C[109], C[110]);
    printf("🟩🟩🟩🟩🟩           ⬜ %s ⬜ 🟨 %s 🟨 ⬜ %s ⬜           🟨🟨🟨🟨🟨\n", C[65], C[91], C[3]);
    printf("🟩%s🟩%s🟩           ⬜ %s ⬜ 🟨 %s 🟨 ⬜ %s ⬜           🟨%s🟨%s🟨\n", C[107], C[108], C[66], C[90], C[2], C[111], C[112]);
    printf("🟩🟩🟩🟩🟩           ⬜ %s ⬜ ⬛ %s ⬛ ⬜ %s ⬜           🟨🟨🟨🟨🟨\n", C[67], C[68], C[1]);
}


/*Auteur : raphael janini*/
/*Date : 13/06/2024*/
/*Resume : fonction qui affiche un de de face n*/
/*Entree : un int n de 1-6*/
/*Sortie : */
void printDe(int n){
    switch (n){
        case 1: printf("⬜⬜⬜⬜⬜⬜⬜\n");
                printf("⬜⬜⬜⬜⬜⬜⬜\n");
                printf("⬜⬜⬜⬜⬜⬜⬜\n");
                printf("⬜⬜⬜⬛⬜⬜⬜\n");
                printf("⬜⬜⬜⬜⬜⬜⬜\n");
                printf("⬜⬜⬜⬜⬜⬜⬜\n");
                printf("⬜⬜⬜⬜⬜⬜⬜\n");break;

        case 2: printf("⬜⬜⬜⬜⬜⬜⬜\n");
                printf("⬜⬜⬜⬜⬜⬛⬜\n");
                printf("⬜⬜⬜⬜⬜⬜⬜\n");
                printf("⬜⬜⬜⬜⬜⬜⬜\n");
                printf("⬜⬜⬜⬜⬜⬜⬜\n");
                printf("⬜⬛⬜⬜⬜⬜⬜\n");
                printf("⬜⬜⬜⬜⬜⬜⬜\n");break;

        case 3: printf("⬜⬜⬜⬜⬜⬜⬜\n");
                printf("⬜⬛⬜⬜⬜⬜⬜\n");
                printf("⬜⬜⬜⬜⬜⬜⬜\n");
                printf("⬜⬜⬜⬛⬜⬜⬜\n");
                printf("⬜⬜⬜⬜⬜⬜⬜\n");
                printf("⬜⬜⬜⬜⬜⬛⬜\n");
                printf("⬜⬜⬜⬜⬜⬜⬜\n");break;

        case 4: printf("⬜⬜⬜⬜⬜⬜⬜\n");
                printf("⬜⬛⬜⬜⬜⬛⬜\n");
                printf("⬜⬜⬜⬜⬜⬜⬜\n");
                printf("⬜⬜⬜⬜⬜⬜⬜\n");
                printf("⬜⬜⬜⬜⬜⬜⬜\n");
                printf("⬜⬛⬜⬜⬜⬛⬜\n");
                printf("⬜⬜⬜⬜⬜⬜⬜\n");break;

        case 5: printf("⬜⬜⬜⬜⬜⬜⬜\n");
                printf("⬜⬛⬜⬜⬜⬛⬜\n");
                printf("⬜⬜⬜⬜⬜⬜⬜\n");
                printf("⬜⬜⬜⬛⬜⬜⬜\n");
                printf("⬜⬜⬜⬜⬜⬜⬜\n");
                printf("⬜⬛⬜⬜⬜⬛⬜\n");
                printf("⬜⬜⬜⬜⬜⬜⬜\n");break;

        case 6: printf("⬜⬜⬜⬜⬜⬜⬜\n");
                printf("⬜⬛⬜⬜⬜⬛⬜\n");
                printf("⬜⬜⬜⬜⬜⬜⬜\n");
                printf("⬜⬛⬜⬜⬜⬛⬜\n");
                printf("⬜⬜⬜⬜⬜⬜⬜\n");
                printf("⬜⬛⬜⬜⬜⬛⬜\n");
                printf("⬜⬜⬜⬜⬜⬜⬜\n");break;
        default: printf("ERREUR D'affichage de de");break;   
    }
}