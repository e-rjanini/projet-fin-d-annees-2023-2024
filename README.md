----------------
Projet De fin d'annees 
----------------

Jeu de Parcheesi.



Instruction d'utilisation (sur linux):
- installer le fichier exe
- lancer le ficher dans la terminal `./exe`

ou

- cloner le projet `git clone <lien SSH>`
- puis utiliser le makefile pour crees le exe `make`
- lancer le exe `./exe`



Règles du Parcheesi :
- 12 cases sont des cases refuges (impossible de se faire manger dessus)
- Le joueur bleu (le plus petit en âge) commence
- On tourne dans le sens des aiguilles d'une montre
- On avance en lançant le dé chacun son tour
- Pour sortir de la base, il faut faire un 6
- On rejoue à chaque 6. Si l'on fait 3 six d'affilée, notre pion rentre à la base et on passe notre tour
- Deux pions du même joueur sur une case forment une barrière infranchissable
- Si l'on fait un 6 et que l'on ne casse pas notre barrière, le pion joué retourne à la base
- Pour rentrer son pion à l'arrivée, il faut faire le chiffre exact
- Si aucun pion n'est déplaçable, le joueur passe son tour
- Manger n'est pas obligatoire
- Quand on mange un pion, on doit faire avancer de 20 cases un pion au choix


Membre du groupe :
- Caudron Thomas 
- Janini Raphael
- Guillou Matéo 
- Richard Emil

--------------------------
©  CY-Tech - pre-ing 1 <3
--------------------------