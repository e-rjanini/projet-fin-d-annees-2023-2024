#ifndef __fonction_H_
#define __fonction_H_
#include "joueur.h"
void tourDejeux(Joueur** tabJoueur); 

int de();

Joueur* Test();

Joueur allouerJoueur();

void initialiserJoueur(Joueur joueur, int nb);

void InitialiseCouleur(Joueur* tabJoueur);

int conditionFinDeJeux(Joueur* tabJoueur);

int ChoisiPion (Joueur* J, int indiceJ, int ResDe, int estRapide);

int EstBarrage(Joueur joueur);

int TourDeJeux(Joueur* J, int estRapide);

int nbPionsALaBase(Joueur J);

int ChoixEchelle(Joueur* J, int i, int PosChoisie, int ResDe, int estRapide);

void InitialiserOrdi (Joueur joueur, int nb);

#endif
