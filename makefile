exe: main.o fonction.o mouvement.o affichage.o
	gcc main.o fonction.o mouvement.o affichage.o -o exe

main.o: main.c fonction.h joueur.h mouvement.h affichage.h
	gcc -c -Wall main.c -o main.o

affichage.o: affichage.c joueur.h affichage.h
	gcc -c -Wall affichage.c -o affichage.o

mouvement.o: mouvement.c joueur.h mouvement.h
	gcc -c -Wall mouvement.c -o mouvement.o

fonction.o: fonction.c joueur.h fonction.h affichage.h
	gcc -c -Wall fonction.c -o fonction.o

clean:
	rm -f *.o